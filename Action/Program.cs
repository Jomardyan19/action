﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Action
{
    class Program
    {
        static void Main(string[] args)
        {
            Action<int, int, int, int> action = (a, b, c, d) =>
              {
                  int res = a * b * c * d;
                  Console.WriteLine($"{a} * {b} * {c} * {d} = {res}");
              }; 

            action(5, 25, 19, 3);

            Console.ReadLine();
        }
    }
}
